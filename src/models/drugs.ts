import { Knex } from 'knex';
export class DrugsModel {

  info(db: Knex,hospcode:any) {
    let sql = db('drugs')
    return sql.select()
    .where('hospcode',hospcode)
    .orderBy('seq');

  }

  infoTotal(db: Knex,hospcode:any) {
    let sql = db('drugs')
    return sql.count({ total: '*' })
    .where('hospcode',hospcode);
  }
  // info drug opd
  infoByID(db: Knex,seq:any,hospcode:any) {
    let sql = db('drugs')
    return sql.select(db.raw('drugs.*'),'l_hospital.hospname')
    .leftJoin('l_hospital','l_hospital.hospcode','drugs.hospcode')
    .where('drugs.seq',seq)
    .where('drugs.hospcode',hospcode)
    // .orderBy('drugs.seq')
  }

  // info drug ipd
  infoIPByID(db: Knex,seq:any,hospcode:any) {
    let sql = db('drugs')
    return sql.select('drugs.hospcode','drugs.seq','drugs.drug_name','drugs.drug_usage','drugs.unit','l_hospital.hospname')
    .sum('drugs.qty as qty')
    .min('drugs.date_serv as date_start')
    .max('drugs.date_serv as date_end')
    .leftJoin('l_hospital','l_hospital.hospcode','drugs.hospcode')
    .where('drugs.seq',seq)
    .where('drugs.hospcode',hospcode)
    .groupBy('drugs.hospcode','drugs.seq','drugs.drug_name','drugs.drug_usage','drugs.unit','l_hospital.hospname')
    .orderBy('date_start')
  }

  save(db: Knex,data:object){
    let sql = db('drugs')
    return sql.insert(data)
    .onConflict(['hospcode', 'seq', 'drug_name', 'date_serv', 'time_serv'])
    .merge()
    .returning('*');
  }

  update(db: Knex,data:object,seq:any,hospcode:any){
    let sql = db('drugs')
    return sql.update(data).where('seq',seq).where('hospcode',hospcode);
  }

  delete(db: Knex,seq:any,hospcode:any){
    let sql = db('drugs')
    return sql.delete().where('seq',seq).where('hospcode',hospcode);
  }

}