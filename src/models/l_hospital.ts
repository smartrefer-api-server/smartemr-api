import { Knex } from 'knex';
export class HospitalModel {

  info(db: Knex) {
    let sql = db('l_hospital')
    return sql.select()
    .orderBy('hospcode');

  }

  infoTotal(db: Knex) {
    let sql = db('l_hospital')
    return sql.count({ total: '*' });
  }

  infoByID(db: Knex,hospcode:any) {
    let sql = db('l_hospital')
    return sql.select('*')
    .where('l_hospital.hospcode',hospcode)
  }

}