import { Knex } from 'knex';
export class OpdReviewModel {

  info(db: Knex,hospcode:any) {
    let sql = db('opd_review')
    return sql.select()
    .where('hospcode',hospcode)
    .orderBy('seq');

  }

  infoTotal(db: Knex,hospcode:any) {
    let sql = db('opd_review')
    return sql.count({ total: '*' })
    .where('hospcode',hospcode);
  }

  infoByID(db: Knex,seq:any,hospcode:any) {
    let sql = db('opd_review')
    return sql.select(db.raw('opd_review.*'),'l_hospital.hospname')
    .leftJoin('l_hospital','l_hospital.hospcode','opd_review.hospcode')
    .where('opd_review.seq',seq)
    .where('opd_review.hospcode',hospcode)
    // .orderBy('opd_review.seq')
  }

  save(db: Knex,data:object){
    let sql = db('opd_review')
    return sql.insert(data)
    .onConflict(['hospcode', 'seq'])
    .merge()
    .returning('*');
  }

  update(db: Knex,data:object,seq:any,hospcode:any){
    let sql = db('opd_review')
    return sql.update(data).where('seq',seq).where('hospcode',hospcode);
  }

  delete(db: Knex,seq:any,hospcode:any){
    let sql = db('opd_review')
    return sql.delete().where('seq',seq).where('hospcode',hospcode);
  }

}