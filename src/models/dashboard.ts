import { Knex } from 'knex';
export class DashboardModel {

    async getReportHospitalServiceByMonth(db: Knex, hospcode: any) {
        let sql = db('service')
        .where('service.hospcode', hospcode)
        .whereRaw('service.date_serv >= (CURRENT_DATE - INTERVAL 6 MONTH)')
        .select(db.raw('EXTRACT(MONTH FROM service.date_serv) as month'))
        .count('service.seq as total')
        .groupBy('month')
        return sql;
    }

    async getReportHospitalServiceByYear(db: Knex, hospcode: any) {
        let sql = db('service')
        .where('service.hospcode', hospcode)
        .whereRaw('service.date_serv >= (CURRENT_DATE - INTERVAL 1 YEAR)')
        .select(db.raw('EXTRACT(YEAR FROM service.date_serv) as year'))
        .count('service.seq as total')
        .groupBy('year')
        return sql;
    }

    async getReportHospitalServiceByYearMonth(db: Knex, hospcode: any) {
        let sql = db('service')
        .where('service.hospcode', hospcode)
        .whereRaw('service.date_serv >= (CURRENT_DATE - INTERVAL 1 YEAR)')
        .select(db.raw('EXTRACT(MONTH FROM service.date_serv) as month'))
        .select(db.raw('EXTRACT(YEAR FROM service.date_serv) as year'))
        .count('service.seq as total')
        .groupBy('year','month')
        return sql;
    }

    async getReportService(db: Knex) {
        let sql = db('service')
        .innerJoin('l_hospital', 'service.hospcode', 'l_hospital.hospcode')
        .select('service.hospcode as code')
        .select('l_hospital.hospname as name')
        .select(db.raw('EXTRACT(YEAR FROM service.date_serv) as year'))
        .select(db.raw('EXTRACT(MONTH FROM service.date_serv) as month'))
        .count('service.seq as total')
        .groupBy('service.hospcode','l_hospital.hospname','year','month')
        .orderBy('year','desc')
        .orderBy('month','desc')
        .orderBy('code','asc')
        return sql;
    }
}