import { Knex } from 'knex';
export class PatientProfileModel {

  info(db: Knex,hospcode:any) {
    let sql = db('patient_profile')
    return sql.select()
    .where('hospcode',hospcode)
    .orderBy('hn');

  }

  infoTotal(db: Knex,hospcode:any) {
    let sql = db('patient_profile')
    return sql.count({ total: '*' })
    .where('hospcode',hospcode);
  }

  infoByID(db: Knex,hn:any,hospcode:any) {
    let sql = db('patient_profile')
    return sql.select(db.raw('patient_profile.*'),'l_hospital.hospname')
    .leftJoin('l_hospital','l_hospital.hospcode','patient_profile.hospcode')
    .where('patient_profile.hn',hn)
    .where('patient_profile.hospcode',hospcode)
  }

  infoByCID(db: Knex,cid:any,hospcode:any) {
    let sql = db('patient_profile')
    return sql.select(db.raw('patient_profile.*'),'l_hospital.hospname')
    .leftJoin('l_hospital','l_hospital.hospcode','patient_profile.hospcode')
    .where('patient_profile.cid',cid)
    .where('patient_profile.hospcode',hospcode)
  }

  save(db: Knex,data:object){
    let sql = db('patient_profile')
    return sql.insert(data)
    .onConflict(['hospcode', 'hn'])
    .merge()
    .returning('*');
  }

  update(db: Knex,data:object,hn:any,hospcode:any){
    let sql = db('patient_profile')
    return sql.update(data).where('hn',hn).where('hospcode',hospcode);
  }

  delete(db: Knex,hn:any,hospcode:any){
    let sql = db('patient_profile')
    return sql.delete().where('hn',hn).where('hospcode',hospcode);
  }

}