import { Knex } from 'knex';
export class LabModel {

  info(db: Knex,hospcode:any) {
    let sql = db('lab')
    return sql.select()
    .where('hospcode',hospcode)
    .orderBy('seq');

  }

  infoTotal(db: Knex,hospcode:any) {
    let sql = db('lab')
    return sql.count({ total: '*' })
    .where('hospcode',hospcode);
  }

  infoByID(db: Knex,seq:any,hospcode:any) {
    let sql = db('lab')
    return sql.select(db.raw('lab.*'),'l_hospital.hospname')
    .leftJoin('l_hospital','l_hospital.hospcode','lab.hospcode')
    .where('lab.seq',seq)
    .where('lab.hospcode',hospcode)
    // .orderBy('lab.seq')
  }

  save(db: Knex,data:object){
    let sql = db('lab')
    return sql.insert(data)
    .onConflict(['hospcode', 'seq', 'date_serv', 'time_serv', 'labgroup', 'lab_name'])
    .merge()
    .returning('*');
  }

  update(db: Knex,data:object,seq:any,hospcode:any){
    let sql = db('lab')
    return sql.update(data).where('seq',seq).where('hospcode',hospcode);
  }

  delete(db: Knex,seq:any,hospcode:any){
    let sql = db('lab')
    return sql.delete().where('seq',seq).where('hospcode',hospcode);
  }

}