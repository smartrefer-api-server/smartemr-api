import { Knex } from 'knex';
export class XrayModel {

  info(db: Knex,hospcode:any) {
    let sql = db('xray')
    return sql.select()
    .where('hospcode',hospcode)
    .orderBy('seq');
  }

  infoTotal(db: Knex,hospcode:any) {
    let sql = db('xray')
    return sql.count({ total: '*' })
    .where('hospcode',hospcode);
  }

  infoByID(db: Knex,seq:any,hospcode:any) {
    let sql = db('xray')
    return sql.select(db.raw('xray.*'),'l_hospital.hospname')
    .leftJoin('l_hospital','l_hospital.hospcode','xray.hospcode')
    .where('xray.seq',seq)
    .where('xray.hospcode',hospcode);
  }

  save(db: Knex,data:object){
    let sql = db('xray')
    return sql.insert(data)
    .onConflict(['hospcode', 'seq', 'xray_name', 'date_serv'])
    .merge()
    .returning('*');
  }

  update(db: Knex,data:object,seq:any,hospcode:any){
    let sql = db('xray')
    return sql.update(data).where('seq',seq).where('hospcode',hospcode);
  }

  delete(db: Knex,seq:any,hospcode:any){
    let sql = db('xray')
    return sql.delete().where('seq',seq).where('hospcode',hospcode);
  }

}