import { Knex } from 'knex';
export class ServiceModel {

  info(db: Knex,hospcode:any) {
    let sql = db('service')
    return sql.select()
    .where('hospcode',hospcode)
    .orderBy('seq');

  }

  infoTotal(db: Knex,hospcode:any) {
    let sql = db('service')
    return sql.count({ total: '*' })
    .where('hospcode',hospcode);
  }

  infoByID(db: Knex,seq:any,hospcode:any) {
    let sql = db('service')
    return sql.select(db.raw('service.*'),'l_hospital.hospname')
    .leftJoin('l_hospital','l_hospital.hospcode','service.hospcode')
    .where('service.seq',seq)
    .where('service.hospcode',hospcode);
  }

  save(db: Knex,data:object){
    let sql = db('service')
    return sql.insert(data)
    .onConflict(['hospcode', 'seq'])
    .merge()
    .returning('*');
  }

  update(db: Knex,data:object,seq:any,hospcode:any){
    let sql = db('service')
    return sql.update(data).where('seq',seq).where('hospcode',hospcode);
  }

  delete(db: Knex,seq:any,hospcode:any){
    let sql = db('service')
    return sql.delete().where('seq',seq).where('hospcode',hospcode);
  }

}