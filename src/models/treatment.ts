import { Knex } from 'knex';
export class TreatmentModel {

  info(db: Knex,hospcode:any) {
    let sql = db('treatment')
    return sql.select()
    .where('hospcode',hospcode)
    .orderBy('seq');

  }

  infoTotal(db: Knex,hospcode:any) {
    let sql = db('treatment')
    return sql.count({ total: '*' })
    .where('hospcode',hospcode);
  }

  infoByID(db: Knex,seq:any,hospcode:any) {
    let sql = db('treatment')
    return sql.select(db.raw('treatment.*'),'l_hospital.hospname')
    .leftJoin('l_hospital','l_hospital.hospcode','treatment.hospcode')
    .where('treatment.seq',seq)
    .where('treatment.hospcode',hospcode);
  }

  save(db: Knex,data:object){
    let sql = db('treatment')
    return sql.insert(data)
    .onConflict(['hospcode', 'seq', 'procedure_code'])
    .merge()
    .returning('*');
  }

  update(db: Knex,data:object,seq:any,hospcode:any){
    let sql = db('treatment')
    return sql.update(data).where('seq',seq).where('hospcode',hospcode);
  }

  delete(db: Knex,seq:any,hospcode:any){
    let sql = db('treatment')
    return sql.delete().where('seq',seq).where('hospcode',hospcode);
  }

}