import { Knex } from 'knex';
export class DiagnosisModel {

  info(db: Knex,hospcode:any) {
    let sql = db('diagnosis')
    return sql.select()
    .where('hospcode',hospcode)
    .orderBy('seq');

  }

  infoTotal(db: Knex,hospcode:any) {
    let sql = db('diagnosis')
    return sql.count({ total: '*' })
    .where('hospcode',hospcode);
  }

  infoByID(db: Knex,seq:any,hospcode:any) {
    let sql = db('diagnosis')
    return sql.select(db.raw('diagnosis.*'),'l_hospital.hospname')
    .leftJoin('l_hospital','l_hospital.hospcode','diagnosis.hospcode')
    .where('diagnosis.seq',seq)
    .where('diagnosis.hospcode',hospcode)
    // .orderBy('diagnosis.seq')
  }

  save(db: Knex,data:object){
    let sql = db('diagnosis')
    return sql.insert(data)
    .onConflict(['hospcode', 'seq', 'icd_code'])
    .merge()
    .returning('*');
  }

  update(db: Knex,data:object,seq:any,hospcode:any){
    let sql = db('diagnosis')
    return sql.update(data).where('seq',seq).where('hospcode',hospcode);
  }

  delete(db: Knex,seq:any,hospcode:any){
    let sql = db('diagnosis')
    return sql.delete().where('seq',seq).where('hospcode',hospcode);
  }

}