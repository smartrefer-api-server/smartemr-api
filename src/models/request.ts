import { Knex } from 'knex';

export class RequestModel {

    async getHospitalByCid(db: Knex, cid: any) {
        let sql = db('service')
        .innerJoin('l_hospital', 'service.hospcode', 'l_hospital.hospcode')
        .innerJoin('patient_profile', function () {
            this.on('service.hn', '=', 'patient_profile.hn')
            this.on('service.hospcode', '=', 'patient_profile.hospcode')
        })
        .max('service.date_serv as last_visit')
        .where('patient_profile.cid', cid)
        .groupBy('l_hospital.hospcode', 'l_hospital.hospname')
        .orderBy('last_visit','desc')
        return sql.select('l_hospital.hospcode', 'l_hospital.hospname as name').count('service.hospcode as total')
    }

    async getService(db: Knex, hospcode: any, cid: any) {
        let sql = db('service')
        .innerJoin('l_hospital', 'service.hospcode', 'l_hospital.hospcode')
        .innerJoin('patient_profile', function () {
            this.on('service.hn', '=', 'patient_profile.hn')
            this.on('service.hospcode', '=', 'patient_profile.hospcode')
        })
        .where('service.hospcode', hospcode)
        .where('patient_profile.cid', cid)
        return sql.select('service.hospcode', 'service.seq as vn', 'service.date_serv as date', 'service.time_serv as time', 'service.hn', 'service.an','service.department_name as clinic', 'service.discharge_status')
        .orderBy('service.date_serv', 'desc')
    }

    async saveLogs(db: Knex, data:object) {
        let sql = db('logs')
            return sql.insert(data)
            .returning('*');
    }

    async reportLogs(db:Knex){
        let sql = db('logs')
        .innerJoin('l_hospital', 'logs.hospcode', 'l_hospital.hospcode')
        return sql.select('logs.hospcode', 'l_hospital.hospname as name').count('logs.hospcode as total')
        .groupBy('logs.hospcode', 'l_hospital.hospname')
    }

    async reportLogsService(db: Knex){
        let sql = db('logs')
        .innerJoin('l_hospital', 'logs.hospcode', 'l_hospital.hospcode')
        .whereLike('logs.log_detail','Read%')
        return sql.select('logs.hospcode', 'l_hospital.hospname as name').count('logs.hospcode as total')
        .groupBy('logs.hospcode', 'l_hospital.hospname')
    }

    async reportLogAccess(db: Knex){
        let sql = db('logs')
        .innerJoin('l_hospital', 'logs.hospcode', 'l_hospital.hospcode')
        .whereLike('logs.log_detail','Access%')
        return sql.select('logs.hospcode', 'l_hospital.hospname as name').count('logs.hospcode as total')
        .groupBy('logs.hospcode', 'l_hospital.hospname')
    }
}