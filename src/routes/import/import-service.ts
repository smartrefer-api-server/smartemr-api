import { FastifyInstance, FastifyReply, FastifyRequest } from 'fastify';
import { getReasonPhrase, StatusCodes } from 'http-status-codes';
import { Knex } from 'knex';
import _ from 'lodash';
import { DiagnosisModel } from '../../models/diagnosis';
import { DrugsModel } from '../../models/drugs';
import { LabModel } from '../../models/lab';
import { OpdReviewModel } from '../../models/opd_review';
import { PatientProfileModel } from '../../models/patient_profile';
import { ServiceModel } from '../../models/service';
import { TreatmentModel } from '../../models/treatment';
import { XrayModel } from '../../models/xray';



// schema
import createSchema from '../../schema/user/list';

export default async (fastify: FastifyInstance, _options: any, done: any) => {

  const db: Knex = fastify.db;
  const diagnosisModel = new DiagnosisModel();
  const drugsModel = new DrugsModel();
  const labModel = new LabModel();
  const opdReviewModel = new OpdReviewModel();
  const patientProfileModel = new PatientProfileModel();
  const serviceModel = new ServiceModel();
  const ireatmentModel = new TreatmentModel();
  const xrayModel = new XrayModel();

  //List
  fastify.post('/', {
    preHandler: [],
  }, async (request: FastifyRequest, reply: FastifyReply) => {
    const req:any = request;
    const body:any = req.body;
    // console.log(body.profile);
    try {
        let datas:any = {} ;
        let visit:any = body.visit[0];
        // console.log('visit : ',visit);
        
        let _visit:any = {
            "hospcode": visit.hospcode,
            "seq": visit.seq,
            "hn": visit.hn,
            "an": visit.an,
            "date_serv": visit.date_serv,
            "time_serv": visit.time_serv,
            "discharge_status": visit.discharge_status,
            "department_name": visit.department
        }        
            datas.service = _visit;
            let rs_visit:any=await serviceModel.save(db,_visit);
            if(rs_visit){
                let profile:any = body.profile[0];
                let register_date:any;
                if(profile.registdate == 'Invalid date'){
                    register_date = null
                }else{
                    register_date = profile.registdate
                }
                
                let _patient_profile:any = {
                        "hospcode": profile.hospcode,
                        "hn": profile.hn,
                        "cid": profile.cid,
                        "title_name": profile.title_name,
                        "first_name": profile.first_name,
                        "last_name": profile.last_name,
                        "address": profile.addrpart,
                        "telephone": profile.telephone,
                        "village_code": profile.moopart,
                        "subdistrict_code": profile.tmbpart,
                        "district_code": profile.amppart,
                        "province_code": profile.chwpart,
                        "dob": profile.brthdate,
                        "sex": profile.sex,
                        "occupation": profile.occupation,
                        "pttype_code": profile.pttype_id,
                        "pttype_name": profile.pttype_name,
                        "pttype_no": profile.pttype_no,
                        "hospmain": profile.hospmain,
                        "hospmain_name": profile.hospmain_name,
                        "hospsub": profile.hospsub,
                        "hospsub_name": profile.hospsub_name,
                        "father_name": profile.father_name,
                        "mother_name": profile.mother_name,
                        "couple_name": profile.couple_name,
                        "contact_name": profile.contact_name,
                        "contact_relation": profile.contact_relation,
                        "contact_mobile": profile.contact_mobile,
                        "contact_address": profile.contact_address,
                        "register_date": register_date,
                        "last_visit_date": profile.visitdate
                }
                datas.patient_profile = _patient_profile;

                let rs_patient:any= await patientProfileModel.save(db,_patient_profile);
    
                let nurtures:any = body.nurtures[0];
                let pillness:any;
                if(nurtures){

                    if(body.pillness[0]){
                        pillness = body.pillness[0].pi;
                    }else{
                        pillness = null
                    }
                    let physicals:any
                    if(body.physicals[0]){
                        physicals = body.physicals[0].pe;
                    }else{
                        physicals = null
                    }
                    let _opd_review:any = {
                            "hospcode": nurtures.hospcode,
                            "seq": nurtures.seq,
                            "weight": nurtures.weight || null,
                            "height": nurtures.height || null,
                            "bmi": nurtures.bmi || null,
                            "temperature": nurtures.temperature || null,
                            "pr": nurtures.pr || null,
                            "rr": nurtures.rr || null,
                            "sbp": nurtures.sbp || null,
                            "dbp": nurtures.dbp || null,
                            "cc": nurtures.symptom,
                            "pi": pillness,
                            "pe": physicals,
                            "ph": nurtures.ph,
                            "movement_score": nurtures.movement_score || null,
                            "vocal_score": nurtures.vocal_score || null,
                            "eye_score": nurtures.eye_score || null, 
                            "oxygen_sat": nurtures.oxygen_sat || null,
                            "gak_coma_score": nurtures.gak_coma_sco || null,
                            "pupil_right": nurtures.pupil_right,
                            "pupil_left": nurtures.pupil_left,
                            "diag_text": nurtures.diag_text
                    }
        
                    datas.opd_review = _opd_review;
    
                    // console.log(_opd_review);
                    
                    let rs_opdreview:any = await opdReviewModel.save(db,_opd_review);
    
                }

                let _diagnosis_:any = [];
                if(body.diagnosis[0]){
                    for(let x of body.diagnosis){
                        let _diagnosis:any = {
                            "hospcode": x.hospcode,
                            "seq": x.seq,
                            "icd_code": x.icd_code,
                            "icd_name": x.icd_name,
                            "diag_type": x.diag_type,
                            "diag_note": x.diag_note,
                            "diagtype_id": x.diagtype_id
                        }
                        _diagnosis_.push(_diagnosis);
                        let rs_diagnosis:any = await diagnosisModel.save(db,_diagnosis);
                    }
                }else{
                    _diagnosis_=[];
                }
                datas.diagnosis = _diagnosis_;

                let _procedure_:any = [];
                
                if(body.procedure[0]){
                    for(let x of body.procedure){
                        let start_date:any;
                        if(x.start_date == 'Invalid date'){
                            start_date = null;
                        }else{
                            start_date = x.start_date;
                        }
                        let _procedure:any = {
                            "hospcode": x.hospcode,
                            "seq": x.seq,
                            "procedure_code": x.procedure_code,
                            "procedure_name": x.procedure_name,
                            "start_date": start_date,
                            "start_time": x.start_time,
                            "end_date": x.end_date,
                            "end_time": x.end_time
                        }
                        _procedure_.push(_procedure);
    
                        let rs_diagnosis:any = await ireatmentModel.save(db,_procedure);
                    }
                }else{
                    _procedure_=[];
                }
                datas.procedure = _procedure_;

                let _drugs_:any = [];
                if(body.drugs[0]){
                    for(let x of body.drugs){

                        let date_serv:any;
                        if(x.date_serv == 'Invalid date'){
                            date_serv = null;
                        }else{
                            date_serv = x.date_serv;
                        }
                        let _drugs:any = {
                            "hospcode": x.hospcode,
                            "seq": x.seq,
                            "date_serv": date_serv,
                            "time_serv": x.time_serv,
                            "drug_name": x.drug_name,
                            "qty": x.qty,
                            "unit": x.unit,
                            "drug_usage": x.usage_line1 + ' ' + x.usage_line2 + ' ' + x.usage_line3,
                        }
                        _drugs_.push(_drugs);
                        let rs_drugs:any = await drugsModel.save(db,_drugs);
                    }
                }else{
                    _drugs_=[];
                }

                datas.drugs = _drugs_;

                let _lab_:any = [];
                if(body.lab[0]){
                    for(let x of body.lab){
                        let date_serv:any;
                        if(x.date_serv == 'Invalid date'){
                            date_serv = null;
                        }else{
                            date_serv = x.date_serv;
                        }
                        let _lab:any = {
                            "hospcode": x.hospcode,
                            "seq": x.seq,
                            "date_serv": date_serv,
                            "time_serv": x.time_serv,
                            "labgroup": x.labgroup,
                            "lab_name": x.lab_name,
                            "lab_result": x.lab_result,
                            "unit": x.unit,
                            "standard_result": x.standard_result
                        }
                        _lab_.push(_lab);
    
                        let rs_lab:any = await labModel.save(db,_lab);
                    }
                    }else{
                    _lab_=[];
                }
                datas.lab = _lab_;

                let _xray_:any = [];

                if(body.xray[0]){
                    for(let x of body.xray){
                        let xray_date:any;
                        if(x.xray_date == 'Invalid date'){
                            xray_date = null;
                        }else{
                            xray_date = x.xray_date;
                        }
                        let _xray:any = {
                            "hospcode": x.hospcode,
                            "seq": x.seq,
                            "date_serv": xray_date,
                            "xray_name": x.xray_name,
                            "xray_result": x.xray_result
                        }
                        _xray_.push(_xray);
    
                        let rs_xray:any = await xrayModel.save(db,_xray);
                    }                
                }else{
                    _xray_=[];
                }

                datas.xray = _xray_;

                reply.status(StatusCodes.CREATED).send({ ok: true ,result:datas});

            }

    
        } catch (error:any) {
            reply.status(StatusCodes.CREATED).send({ ok: false ,result:error});
            
        }


  })

  done();

} 
