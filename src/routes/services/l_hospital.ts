import { FastifyInstance, FastifyReply, FastifyRequest } from 'fastify';
import { getReasonPhrase, StatusCodes } from 'http-status-codes';
import { Knex } from 'knex';
import _ from 'lodash';
import { HospitalModel } from '../../models/l_hospital';

// schema
import createSchema from '../../schema/user/list';

export default async (fastify: FastifyInstance, _options: any, done: any) => {

  const db: Knex = fastify.db;
  const importService = new HospitalModel();

  //List
  fastify.get('/', {
    preHandler: [],
  }, async (request: FastifyRequest, reply: FastifyReply) => {
    const req:any = request;
    try {
      const data:any = await importService.info(db);
      const rsTotal: any = await importService.infoTotal(db);
      return reply.status(StatusCodes.CREATED).send({ ok: true ,data,total: Number(rsTotal[0].total)});
    } catch (error: any) {
      request.log.info(error.message);
      return reply.status(StatusCodes.INTERNAL_SERVER_ERROR).send({ ok: false, error: 'เกิดข้อผิดพลาด' });
    }
  })

  //List By Id
  fastify.get('/:seq', {
    preHandler: [],
  }, async (request: FastifyRequest, reply: FastifyReply) => {
    const req:any = request;
    const seq:any = req.params.seq;
    try {
      const data:any = await importService.infoByID(db,seq);

      return reply.status(StatusCodes.CREATED).send({ ok: true ,data});

    } catch (error: any) {
      request.log.info(error.message);
      return reply.status(StatusCodes.INTERNAL_SERVER_ERROR).send({ ok: false, error: 'เกิดข้อผิดพลาด' });
    }
  })



  done();

} 
