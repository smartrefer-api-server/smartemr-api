import { FastifyInstance } from "fastify";

export default async (fastify: FastifyInstance, _options: any, done: any) => {

  // verify jwt token
  // fastify.addHook("onRequest", (request) => request.jwtVerify());

  fastify.register(require('./test'), { prefix: '/' });
  fastify.register(require('./diagnosis'), { prefix: '/diagnosis' });
  fastify.register(require('./drugs'), { prefix: '/drugs' });
  fastify.register(require('./l_hospital'), { prefix: '/hospital' });
  fastify.register(require('./lab'), { prefix: '/lab' });
  fastify.register(require('./opd_review'), { prefix: '/opd_review' });
  fastify.register(require('./patient_profile'), { prefix: '/patient_profile' });
  fastify.register(require('./service'), { prefix: '/service' });
  fastify.register(require('./treatment'), { prefix: '/treatment' });
  fastify.register(require('./xray'), { prefix: '/xray' });

  done();

} 
