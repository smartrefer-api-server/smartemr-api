import { FastifyInstance, FastifyReply, FastifyRequest } from 'fastify';
import { getReasonPhrase, StatusCodes } from 'http-status-codes';
import { Knex } from 'knex';
import _ from 'lodash';
import { DrugsModel } from '../../models/drugs';

// schema
import createSchema from '../../schema/user/list';

export default async (fastify: FastifyInstance, _options: any, done: any) => {

  const db: Knex = fastify.db;
  const importService = new DrugsModel();

  //List
  fastify.get('/:hospcode', {
    preHandler: [],
  }, async (request: FastifyRequest, reply: FastifyReply) => {
    const req:any = request;
    const hospcode:any = req.params.hospcode;
    try {
      const data:any = await importService.info(db,hospcode);
      const rsTotal: any = await importService.infoTotal(db,hospcode);
      return reply.status(StatusCodes.CREATED).send({ ok: true ,data,total: Number(rsTotal[0].total)});
    } catch (error: any) {
      request.log.info(error.message);
      return reply.status(StatusCodes.INTERNAL_SERVER_ERROR).send({ ok: false, error: 'เกิดข้อผิดพลาด' });
    }
  })

  //List By Id
  fastify.get('/:seq/:hospcode', {
    preHandler: [],
  }, async (request: FastifyRequest, reply: FastifyReply) => {
    const req:any = request;
    const seq:any = req.params.seq;
    const hospcode:any = req.params.hospcode;
    try {
      const data:any = await importService.infoByID(db,seq,hospcode);

      return reply.status(StatusCodes.CREATED).send({ ok: true ,data});

    } catch (error: any) {
      request.log.info(error.message);
      return reply.status(StatusCodes.INTERNAL_SERVER_ERROR).send({ ok: false, error: 'เกิดข้อผิดพลาด' });
    }
  })

  //Save
  fastify.post('/', {
    preHandler: [],
  }, async (request: FastifyRequest, reply: FastifyReply) => {
    const req:any = request;
    const datas:object = req.body;
    try {
      const data:any = await importService.save(db,datas);
      return reply.status(StatusCodes.CREATED).send({ ok: true ,data});
    } catch (error: any) {
      request.log.info(error.message);
      return reply.status(StatusCodes.INTERNAL_SERVER_ERROR).send({ ok: false, error: 'เกิดข้อผิดพลาด' });
    }
  })  

  //Update
  fastify.put('/:seq/:hospcode', {
    preHandler: [],
  }, async (request: FastifyRequest, reply: FastifyReply) => {
    const req:any = request;
    const datas:object = req.body;
    const seq:any = req.params.seq;
    const hospcode:any = req.params.hospcode;

    try {
      const data:any = await importService.update(db,datas,seq,hospcode);
      return reply.status(StatusCodes.CREATED).send({ ok: true ,data});
    } catch (error: any) {
      request.log.info(error.message);
      return reply.status(StatusCodes.INTERNAL_SERVER_ERROR).send({ ok: false, error: 'เกิดข้อผิดพลาด' });
    }
  })  

  //Delete
  fastify.delete('/:seq/:hospcode', {
    preHandler: [],
  }, async (request: FastifyRequest, reply: FastifyReply) => {
    const req:any = request;
    const seq:any = req.params.seq;
    const hospcode:any = req.params.hospcode;

    try {
      const data:any = await importService.delete(db,seq,hospcode);
      return reply.status(StatusCodes.CREATED).send({ ok: true ,data});
    } catch (error: any) {
      request.log.info(error.message);
      return reply.status(StatusCodes.INTERNAL_SERVER_ERROR).send({ ok: false, error: 'เกิดข้อผิดพลาด' });
    }
  }) 

  done();

} 
