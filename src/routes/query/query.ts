import { FastifyInstance } from "fastify";
import { DiagnosisModel } from '../../models/diagnosis';
import { DrugsModel } from '../../models/drugs';
import { HospitalModel } from '../../models/l_hospital';
import { LabModel } from '../../models/lab';
import { OpdReviewModel } from '../../models/opd_review';
import { PatientProfileModel } from '../../models/patient_profile';
import { RequestModel } from "../../models/request";
import { TreatmentModel } from '../../models/treatment';
import { XrayModel } from '../../models/xray';
import { DashboardModel } from "../../models/dashboard";

import { getReasonPhrase, StatusCodes } from 'http-status-codes';
import { Knex } from 'knex';
import _ from 'lodash';


export default async (fastify: FastifyInstance, _options: any, done: any) => {

  const db: Knex = fastify.db;
  const diagService = new DiagnosisModel();
  const drugsService = new DrugsModel();
  const hospitalService = new HospitalModel();
  const labService = new LabModel();
  const opdReviewService = new OpdReviewModel();
  const patientProfileService = new PatientProfileModel();
  const requestService = new RequestModel();
  const treatmentService = new TreatmentModel();
  const xrayService = new XrayModel();
  const dashboardService = new DashboardModel();
  

  fastify.get('/hospital/:cid', { preHandler: [] }, async (request: any, reply: any) => {
    const cid = request.params.cid;
    try {
      let profile : any = {};
      const rs: any = await requestService.getHospitalByCid(db, cid);
      if(rs.length > 0){
        profile = await patientProfileService.infoByCID(db,cid,rs[0].hospcode)
      }
      return reply.status(StatusCodes.CREATED).send({ ok: true, data: rs , profile_data : profile });
    }
    catch (error: any) {
      request.log.info(error.message);
      return reply.status(StatusCodes.INTERNAL_SERVER_ERROR).send({ ok: false, error: 'เกิดข้อผิดพลาด' });
    }
  });

  fastify.get('/service/:hospcode/:cid', { preHandler: [] }, async (request: any, reply: any) => {
    const hospcode = request.params.hospcode;
    const cid = request.params.cid;
    try {
      const rs: any = await requestService.getService(db, hospcode, cid);
      return reply.status(StatusCodes.CREATED).send({ ok: true, data: rs });
    }
    catch (error: any) {
      request.log.info(error.message);
      return reply.status(StatusCodes.INTERNAL_SERVER_ERROR).send({ ok: false, error: 'เกิดข้อผิดพลาด' });
    }
  });

  fastify.get('/service-profile/:hospcode/:hn/:seq', { preHandler: [] }, async (request: any, reply: any) => {
    const hospcode = request.params.hospcode;
    const hn = request.params.hn;
    const seq = request.params.seq;
    try {
      let rs: any = {};
      const rsDiag: any = await diagService.infoByID(db, seq, hospcode);
      const rsDrugs: any = await drugsService.infoByID(db, seq, hospcode);
      const rsLab: any = await labService.infoByID(db, seq, hospcode);
      const rsOpdReview: any = await opdReviewService.infoByID(db, seq, hospcode);
      const rsPatientProfile: any = await patientProfileService.infoByID(db, hn, hospcode);
      const rsTreatment: any = await treatmentService.infoByID(db, seq, hospcode);
      const rsXray: any = await xrayService.infoByID(db, seq, hospcode);
      let rsDrugsIP : any = await drugsService.infoIPByID(db, seq, hospcode);
      
      // set order type
      for(let d of rsDrugsIP){
        // set datestart and dateend
        let datestart = new Date(d.date_start);
        let dateend = new Date(d.date_end);
        // compare date_start and date_end
        if(datestart.getTime() == dateend.getTime()){
          // if date_start == date_end set order_type = ONEDAY
          d.order_type = 'ONEDAY';
        } else {
          // if date_start != date_end set order_type = ONEDAY
          d.order_type = 'CONTINUE';
        }
      }

      rs = {
        diagnosis: rsDiag,
        drugs: rsDrugs,
        orders: rsDrugsIP,
        labs: rsLab,
        visit_detail : {
          cc: rsOpdReview[0].cc,
          pe: rsOpdReview[0].pe,
          pi: rsOpdReview[0].pi,
          ph: rsOpdReview[0].ph,
          vital_sign: {
            ht: rsOpdReview[0].height,
            wt: rsOpdReview[0].weight,
            bmi: rsOpdReview[0].bmi,
            bt: rsOpdReview[0].temperature,
            pr: rsOpdReview[0].pr,
            rr: rsOpdReview[0].rr,
            sbp: rsOpdReview[0].sbp,
            dbp: rsOpdReview[0].dbp,
            o2sat: rsOpdReview[0].oxygen_sat,
            movement_score: rsOpdReview[0].movement_score,
            eye_score: rsOpdReview[0].eye_score,
            verbal_score: rsOpdReview[0].vocal_score,
            gcs_score: rsOpdReview[0].gak_coma_score,
          }
        },
        profile: rsPatientProfile,
        procedures: rsTreatment,
        xrays: rsXray
      }

      return reply.status(StatusCodes.CREATED).send({ ok: true, data: rs });
    }
    catch (error: any) {
      request.log.info(error.message);
      return reply.status(StatusCodes.INTERNAL_SERVER_ERROR).send({ ok: false, error: 'เกิดข้อผิดพลาด' });
    }
  });

fastify.post('/saveLogs', { preHandler: [] }, async (request: any, reply: any) => {
  const data = request.body;
  try {
    const rs: any = await requestService.saveLogs(db, data);
    return reply.status(StatusCodes.CREATED).send({ ok: true, data: rs });
  }
  catch (error: any) {
    request.log.info(error.message);
    return reply.status(StatusCodes.INTERNAL_SERVER_ERROR).send({ ok: false, error: 'เกิดข้อผิดพลาด' });
  }
});

fastify.get('/reportLogs', { preHandler: [] }, async (request: any, reply: any) => {
  try {
    const rs: any = await requestService.reportLogs(db);
    return reply.status(StatusCodes.CREATED).send({ ok: true, data: rs });
  }
  catch (error: any) {
    request.log.info(error.message);
    return reply.status(StatusCodes.INTERNAL_SERVER_ERROR).send({ ok: false, error: 'เกิดข้อผิดพลาด' });
  }
});

fastify.get('/reportLogsService', { preHandler: [] }, async (request: any, reply: any) => {
  try {
    const rs: any = await requestService.reportLogsService(db);
    return reply.status(StatusCodes.CREATED).send({ ok: true, data: rs });
  }
  catch (error: any) {
    request.log.info(error.message);
    return reply.status(StatusCodes.INTERNAL_SERVER_ERROR).send({ ok: false, error: 'เกิดข้อผิดพลาด' });
  }
});

fastify.get('/reportDashboardService', async (request: any, reply: any) => {
  try {
    const rs: any = await dashboardService.getReportService(db);
    return reply.status(StatusCodes.CREATED).send({ ok: true, data: rs });
  }
  catch (error: any) {
    request.log.info(error.message);
    return reply.status(StatusCodes.INTERNAL_SERVER_ERROR).send({ ok: false, error: 'เกิดข้อผิดพลาด' });
  }
});
  // verify jwt token
  // fastify.addHook("onRequest", (request) => request.jwtVerify());

  // fastify.register(require('./'), { prefix: '/' });

  done();

} 
